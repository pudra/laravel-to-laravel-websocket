<?php

namespace App\Jobs;

use App\ClientAuthenticator;
use App\Services\ScoreAPI;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Str;
use Thruway\ClientSession;
use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;
use Exception;

class ListenSocket implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct() { }

    /**
     * @throws Exception
     */
    public function handle() {
        $client = new Client("Soccer90");
        $client->addTransportProvider(new PawlTransportProvider("ws://" . env('API_URL_BASE') . ":"
                                                                . env('WS_PORT') ."/"));

        $client->addClientAuthenticator(new ClientAuthenticator());

        $client->on('open', function (ClientSession $session) {
            $onevent = function ($args) {
                $eventType = 'App\\Events\\' . ucfirst(Str::camel($args[0]->ws_event));

                broadcast(new $eventType($args[0]->fixture));
            };

            $fixtureIds = ScoreAPI::getTodayFixtureIds();
            foreach ($fixtureIds as $currentFixtureId) {
                $session->subscribe("fixture.score." . $currentFixtureId, $onevent);
                $session->subscribe("fixture.events." . $currentFixtureId, $onevent);
                $session->subscribe("fixture.substitutions." . $currentFixtureId, $onevent);
            }
        });

        $client->start();

        throw new Exception();
    }

    public function failed(Exception $exception) {
        $this::dispatch()->onQueue('score-long');
    }


}