<?php

namespace App\Services\LaravelThruway\Broadcaster;

use Illuminate\Broadcasting\Broadcasters\Broadcaster;
use Illuminate\Support\Str;
use App\Services\LaravelThruway\Pusher;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ZmqBroadcaster extends Broadcaster
{
    private $pusher;

    public function __construct(Pusher $pusher) {
        $this->pusher = $pusher;
    }

    public function auth($request) {
        if ( Str::startsWith($request->channel_name, ['private-', 'presence-']) && !$request->user() ) {
            throw new AccessDeniedHttpException;
        }

        $channelName = Str::startsWith($request->channel_name, 'private-')
            ? Str::replaceFirst('private-', '', $request->channel_name)
            : Str::replaceFirst('presence-', '', $request->channel_name);

        return $this->verifyUserCanAccessChannel($request, $channelName);
    }

    public function validAuthenticationResponse($request, $result) {
        if (is_bool($result)) {
            return json_encode($result);
        }

        return json_encode(['channel_data' => [
            'user_id' => $request->user()->getAuthIdentifier(),
            'user_info' => $result,
        ]]);
    }

    public function broadcast(array $channels, $event, array $payload = []) {
        $payload['ws_event'] = $event;

        foreach ($channels as $channel) {
            $this->pusher->push($channel->name, $payload);
        }
    }
}
