<?php

namespace App\Services\LaravelThruway;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Thruway\Logging\Logger;
use ZMQ;
use Thruway\Authentication\AbstractAuthProviderClient;

class Server extends AbstractAuthProviderClient implements ServerInterface
{
    protected $zmqHost = '127.0.0.1';
    protected $zmqPort = 5555;
    protected $zmqSocketType = ZMQ::SOCKET_PULL;

    public function setZmqHost($zmqHost) {
        $this->zmqHost = $zmqHost;
        return $this;
    }


    public function setZmqPort($zmqPort) {
        $this->zmqPort = $zmqPort;
        return $this;
    }

    public function setZmqSocketType($zmqSocketType) {
        $this->zmqSocketType = $zmqSocketType;
        return $this;
    }

    public function getMethodName() {
        return 'oauth2';
    }

    public function processAuthenticate($signature, $extra = null) {
        $client = new Client(['base_uri' => env('APP_URL')]);

        try {
            $response = $client->request('GET', '/api/oauth/ws-auth', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                    'Authorization' => "Bearer $signature",
                ]
            ]);
        } catch (\Exception $exception) {
            return ["FAILURE"];
        }

        $userId = json_decode($response->getBody()->getContents())->user_id;

        return ["SUCCESS", ["authid" => $userId]];
    }

    public function onSessionStart($session, $transport) {
        Logger::info($this, "Client onSessionStart");

        $session->register(
            "thruway.auth.{$this->getMethodName()}.onhello",
            [$this, 'processHello'],
            ['replace_orphaned_session' => 'yes']
        )->then(function () use ($session) {
            $session->register(
                "thruway.auth.{$this->getMethodName()}.onauthenticate",
                [$this, 'preProcessAuthenticate'],
                ['replace_orphaned_session' => 'yes']
            )->then(function () use ($session) {

                $registrations                 = new \stdClass();
                $registrations->onhello        = "thruway.auth.{$this->getMethodName()}.onhello";
                $registrations->onauthenticate = "thruway.auth.{$this->getMethodName()}.onauthenticate";

                $session->call('thruway.auth.registermethod',
                    [
                        $this->getMethodName(),
                        $registrations,
                        $this->getAuthRealms()
                    ]
                )->then(function ($args) {
                    Logger::debug($this, "Authentication Method Registration Successful: {$this->getMethodName()}");
                });
            });
        });
    }

    public function onEntry($msg) {
        Logger::debug($this, "Client onEntry: {$msg}");

        $entryData = json_decode($msg, true);

        if (!isset($entryData['ws_channel'])) {
            return;
        }
        $channel = $entryData['ws_channel'];
        unset($entryData['ws_channel']);

        $this->getSession()->publish($channel, [$entryData]);
    }

    public function createSubscriptions($session, $transport) { }
}
