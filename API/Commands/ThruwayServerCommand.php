<?php

namespace App\Services\LaravelThruway\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Thruway\Logging\Logger;
use Thruway\Peer\Router;
use Thruway\Realm;
use Thruway\Transport\RatchetTransportProvider;
use Thruway\Authentication\AuthenticationManager;
use App\Services\LaravelThruway\Exceptions\ThruwayServerException;
use App\Services\LaravelThruway\BroadcastClient;


class ThruwayServerCommand extends Command
{
    protected $name = 'thruway:serve';
    protected $description = 'Start Thruway Server With Authentication';
    protected $host;
    protected $port;
    protected $realm;
    protected $class;
    protected $router;
    protected $client;
    protected $broadcastClient;

    protected function getOptions() {
        return [
            ['host', null, InputOption::VALUE_OPTIONAL, 'Ratchet server host', config('thruway.host', '0.0.0.0')],
            ['port', 'p', InputOption::VALUE_OPTIONAL, 'Ratchet server port', config('thruway.port', 8080)],
            ['class', null, InputOption::VALUE_OPTIONAL, 'Class that implements PusherInterface.', config('thruway.class')],
            ['realm', null, InputOption::VALUE_OPTIONAL, 'Realm.', config('thruway.realm')],
        ];
    }

    public function handle() {
        $this->host = $this->option('host');
        $this->port = (int) $this->option('port');
        $this->class = $class = $this->option('class');
        $this->realm = $this->option('realm');

        $this->router = new Router();

        $this->router->registerModule(new AuthenticationManager());

        $realmManager = $this->router->getRealmManager();
        $realmManager->addRealm(new Realm('thruway.auth'));
        $realmManager->addRealm(new Realm('ScoreAPI'));
        $realmManager->setAllowRealmAutocreate(false);

        $this->client = new $class([$this->realm], $this->router->getLoop());
        $this->client->setZmqHost(config('thruway.zmq.host', '0.0.0.0'))
            ->setZmqPort(config('thruway.zmq.port', 5555));

        $this->broadcastClient = new BroadcastClient($this->realm, $this->router->getLoop());
        $this->broadcastClient->setZmqHost(config('thruway.zmq.host', '0.0.0.0'))
            ->setZmqPort(config('thruway.zmq.port', 5555));

        $this->router->addInternalClient($this->client);
        $this->router->addInternalClient($this->broadcastClient);

        $this->router->addTransportProvider(new RatchetTransportProvider($this->host, $this->port));

        try {
            $this->router->start();
        } catch (\Exception $e) {
            Logger::error($this, "Cannot start server: {$e->getMessage()}");
            throw new ThruwayServerException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
