<?php

namespace App\Services\LaravelThruway;

use App\Services\LaravelThruway\Exceptions\ThruwayServerException;
use React\ZMQ\Context;
use Thruway\Logging\Logger;
use Thruway\Peer\Client;
use ZMQ;
use Thruway\Authentication\AbstractAuthProviderClient;
use Thruway\Message\WelcomeMessage;

class BroadcastClient extends Client implements ServerInterface
{
    protected $zmqHost = '127.0.0.1';
    protected $zmqPort = 5555;
    protected $zmqSocketType = ZMQ::SOCKET_PULL;

    public function setZmqHost($zmqHost) {
        $this->zmqHost = $zmqHost;
        return $this;
    }

    public function setZmqPort($zmqPort) {
        $this->zmqPort = $zmqPort;
        return $this;
    }

    public function setZmqSocketType($zmqSocketType) {
        $this->zmqSocketType = $zmqSocketType;
        return $this;
    }

    public function onSessionStart($session, $transport) {
        Logger::info($this, "Client onSessionStart");

        $this->createSubscriptions($session, $transport);

        $context = new Context($this->getLoop());
        $pull = $context->getSocket($this->zmqSocketType);
        try {
            $pull->bind("tcp://{$this->zmqHost}:{$this->zmqPort}");
        } catch (\ZMQSocketException $e) {
            Logger::error($this, "Cannot create ZMQ context: {$e->getMessage()}");
            throw new ThruwayServerException($e->getMessage(), $e->getCode(), $e);
        }
        $pull->on('message', [$this, 'onEntry']);
    }

    public function onEntry($msg) {
        Logger::debug($this, "Client onEntry: {$msg}");

        $entryData = json_decode($msg, true);

        if (!isset($entryData['ws_channel'])) {
            return;
        }
        $channel = $entryData['ws_channel'];
        unset($entryData['ws_channel']);

        $this->getSession()->publish($channel, [$entryData]);
    }

    public function createSubscriptions($session, $transport) { }
}
