<?php

namespace App\Services\LaravelThruway;


use App\Services\LaravelThruway\Exceptions\PusherException;

class Pusher
{
    protected $context;
    protected $socket;
    protected $zmqHost = '127.0.0.1';
    protected $zmqPort = 5555;
    protected $zmqSocketType = \ZMQ::SOCKET_PUSH;

    public function __construct($host = '127.0.0.1', $port = 5555) {
        $this->zmqHost = $host;
        $this->zmqPort = $port;
        $this->context = new \ZMQContext();
        try {
            $this->socket = $this->context->getSocket($this->zmqSocketType);
            $this->socket->connect("tcp://{$this->zmqHost}:{$this->zmqPort}");
        } catch (\ZMQSocketException $e) {
            throw new PusherException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function push($channel, $message) {
        if (is_array($message)) {
            $message['ws_channel'] = $channel;
        } elseif (is_object($message)) {
            $message = json_decode(json_encode($message), true);
            $message['ws_channel'] = $channel;
        } else {
            $message = [
                'ws_channel' => $channel,
                $message
            ];
        }

        try {
            $this->socket->send(json_encode($message));
        } catch (\ZMQSocketException $e) {
            throw new PusherException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
