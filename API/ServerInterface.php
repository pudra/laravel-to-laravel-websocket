<?php

namespace App\Services\LaravelThruway;

interface ServerInterface
{
    public function onEntry($msg);

    public function createSubscriptions($session, $transport);
}
